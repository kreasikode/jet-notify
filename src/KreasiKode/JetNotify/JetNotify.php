<?php namespace KreasiKode\JetNotify;

use Illuminate\Foundation\Application;
use Illuminate\Session\Store;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Contracts\RenderableInterface;

/**
 * Part of the JetNotify Package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the MIT License.
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://opensource.org/licenses/MIT
 *
 * @package    JetNotify
 * @version    0.2
 * @author     Wuri Nugrahadi
 * @license    MIT
 * @link       http://empu.web.id
 */


class JetNotify extends MessageBag implements RenderableInterface
{
	/**
	 * The Application instance.
	 * @var Illuminate\Foundation\Application
	 */
	protected $app;

	protected $old_messages;

	/**
	 * The key name for our flash session
	 * @var string
	 */
	protected $session_key = 'flash_messages';

	/**
	 * Create a new notify instance
	 *
	 * @param Illuminate\Foundation\Application $app
	 * @return void
	 */
	public function __construct(Application $app)
	{
		$this->app = $app;

		$messages = array();
		// dump messages on flash session
        if ($app['session']->has($this->session_key))
        {
            $messages = $app['session']->get($this->session_key);
        }
        // pass messages to MessageBag
		$this->old_messages = new MessageBag($messages);

        // set default notification format from the configuration
        $format = $app['config']->get('notify::format', ':message');
        $this->setFormat($format);
	}

	/**
	 * Overriding MessageBag::add(), with logging & flashing ability
	 *
	 * @param string $key
	 * @param string $message
	 * @param boolean $log
	 * @return JetNotify
	 */
	public function add($key, $message, $log = false)
	{
		$app = $this->app;

		if ($this->isUnique($key, $message))
		{
			$this->messages[$key][] = $message;
		}

        // check to see if we need to log this message
		if ($log === true)
		{
			$this->logMessage($key, $message);
		}
		// put messages to flash session
		$this->flashMessages();

		return $this;
	}

	protected function logMessage($key, $message)
	{
		$app = $this->app;

		$method = 'add'.ucfirst($key);
		$app['log']->getMonolog()->$method($message);
	}

	protected function flashMessages()
	{
		$app = $this->app;

		$app['session']->flash($this->session_key, $this->messages);
	}

	public function reflash()
	{
		$app = $this->app;

		$this->merge($this->old_messages->toArray());
		$app['session']->flash($this->session_key, $this->messages);
	}

    /**
	 * Get all of the messages from the bag for a given key.
	 *
	 * @param  string  $key
	 * @param  string  $format
	 * @return array
	 */
	public function get($key, $format = null)
	{
		$format = $this->checkNotificationFormat($key, $format);
		// If the message exists in the container, we will transform it and return
		// the message. Otherwise, we'll return an empty array since the entire
		// methods is to return back an array of messages in the first place.
		if (array_key_exists($key, $this->old_messages->messages))
		{
			return $this->transform($this->old_messages->messages[$key], $format, $key);
		}
		return array();
	}

    /**
     * Get all of the messages for every key in the bag.
     *
     * @param  string  $format
     * @return array
     */
	public function all($format = null)
    {
        $all = array();
        $format = array();
        foreach ($this->old_messages->messages as $key => $messages)
        {
        	if (! isset($format[$key])) {
        		$format[$key] = $this->checkNotificationFormat($key, $format);
        	}
            $all = array_merge($all, $this->transform($messages, $format[$key], $key));
        }

        return $all;
    }

	protected function checkNotificationFormat($key, $format)
	{
		$app = $this->app;
		$custom_format = $app['config']->get('notify::format-'.$key);
		$format = $custom_format === false ? '' : $custom_format;
		return $this->checkFormat($format);
	}

	public function render()
	{
		if (! $this->any()) {
			return;
		}

		$list = '';
		foreach ($this->all() as $message) {
				$list .= "<li>{$message}</li>\n";
		}
		return "<ul>{$list}</ul>";
	}

	public function __call($name, $args)
	{
		if (preg_match('~(add|get)(\w+)~', $name, $m)) {
			return call_user_func_array(array($this, $m[1]), array_merge(array($m[2]), $args));
		}

		throw new \BadMethodCallException("Method [$method] does not exist.");
	}

}