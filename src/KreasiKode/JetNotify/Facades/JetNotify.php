<?php namespace KreasiKode\JetNotify\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Part of the JetNotify Package.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the MIT License.
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.  It is also available at
 * the following URL: http://opensource.org/licenses/MIT
 *
 * @package    JetNotify
 * @version    0.2
 * @author     Wuri Nugrahadi
 * @license    MIT
 * @link       http://empu.web.id
 */

class JetNotify extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'notify';
	}

}