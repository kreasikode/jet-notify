<?php

return array(
    'format' => '<div class="alert alert-:key">:message</div>',
    // -- if has custom format --
    // posible format level following log error level
    // 'debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'
    'format-emergency' => '<div class="alert alert-block alert-error"><strong>Emergency!!!</strong> :message</div>',
    // set false if doesn't want error displayed
    'format-debug' => false,
    // for rendering view
    // 'template' =>
);