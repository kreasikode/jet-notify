Laravel 4 Jet Notify
====================
This is a simple messages container for Laravel 4. It allows you to add a message to a container and then either simply get it or render it.

Installing
----------
Super simple to get this up and going.

1. Add ``` "kreasi-kode/jet-notify": "0.3.*" ``` to your composer.json
2. From the command line run ``` php composer.phar update ```
3. Add ``` 'KreasiKode\JetNotify\JetNotifyServiceProvider' ``` to your list of service providers in ```app/config/app.php```
4. Add ```'Notify' => 'KreasiKode\JetNotify\Facades\JetNotify'``` to the list of class aliases in ```app/config/app.php```

Messages should now be up and running.

Using Messages
--------------

 JetNotify class extending Illuminate\Support\MessageBag.

### Adding Messages

```Notify::add($container, $message, $log = false)```

Possible container value is same as log error level, there are:
'debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'.

Or, use shortcut `Notify::addInfo($message)`.


### Retrieving Messages

``Messages::get($container = 'default')``
Grabs an array containing the message and attributes for a container.

``$all messages = Messages::all();``

### Rendering Messages

```Messages::render()```
Renders the message.